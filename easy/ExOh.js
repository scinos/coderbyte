function ExOh(str) {

  var numX = 0
  var numO = 0
  for (var i = 0; i<str.length; i++) {
    if (str[i] == "x") numX++
    else numO++
  }

  return numX == numO
}