function CountingMinutesI(str) {

  function strToMinutes(str){
    var minutes = 0;
    var components = str.match(/([0-9]{1,2}):([0-9]{2})(am|pm)/);
    if (components[1]==12) components[1]=0;
    minutes += Number(components[1])*60;
    minutes += Number(components[2]);
    if (components[3]=="pm" ) minutes += 12*60;
    return minutes;
  }

  var times = str.split("-");
  var time1 = strToMinutes(times[0]);
  var time2 = strToMinutes(times[1]);

  if (time2>=time1) {
    return time2-time1;
  }else{
    return 1440 + (time2-time1);
  }
}