function ArithGeo(arr) {

  function checkAr(arr) {
    var n1 = arr[0]
    var n2 = arr[1]
    var D = n2 - n1

    for (var i=2; i<arr.length; i++) {
      var d = arr[i] - arr[i-1]
      if (D !== d) return false
    }

    return true
  }

  function checkGe(arr) {
    var n1 = arr[0]
    var n2 = arr[1]
    var D = n2/n1

    for (var i=2; i<arr.length; i++) {
      var d = arr[i] / arr[i-1]
      if (D !== d) return false
    }

    return true
  }

  if (checkAr(arr)) return "Arithmetic"
  if (checkGe(arr)) return "Geometric"
  return -1
}
