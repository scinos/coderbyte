function VowelCount(str) {
  return str.replace(/[^aeiou]/gi, "").length
}