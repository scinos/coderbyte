function ArrayAdditionI(arr) {
  var largest = -Infinity

  for (var i =0; i<arr.length; i++) {
    largest = Math.max(largest, arr[i])
  }

  var map = Math.pow(2, arr.length)

  for (var i = 0; i<map; i++) {
    var mask = (new Array(arr.length +1 - i.toString(2).length)).join("0") + i.toString(2)

    var result = 0
    for (var h = 0; h<mask.length; h++) {
      if (arr[h]==largest) continue

      if (mask[h]=="1") result = result+arr[h]
      if (result == largest) return "true"
    }
  }

  return "false"
}
