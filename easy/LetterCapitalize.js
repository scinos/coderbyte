function LetterCapitalize(str) {
  return str.replace(/\b([a-z])/g, function(a) {
    return a.toUpperCase()
  })
}