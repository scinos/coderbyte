function DashInsertII(num) {
  var dashedString = "";
  var oddRe = /[13579]/;
  var evenRe = /[2468]/;
  var str = String(num);

  for (var i = 0; i<str.length; i++) {
    var current = String(str[i]);
    var next = String(str[i+1]);
    dashedString += current;
    if (current.match(oddRe) && next.match(oddRe)) {
      dashedString += "-";
    }else if (current.match(evenRe) && next.match(evenRe)) {
      dashedString += "*";
    }
  }

  return dashedString
}