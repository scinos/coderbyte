function LongestWord(sen) {
  return sen
    .replace(/[^a-z0-9 ]/gi, "")
    .split(" ")
    .sort(function(a,b) {
      return b.length - a.length
    })[0]
}