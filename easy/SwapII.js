function SwapII(str) {
  return str
    .replace(/[a-z]/gi, function(ch) {
      if (ch.match(/[a-z]/)) return ch.toUpperCase();
      if (ch.match(/[A-Z]/)) return ch.toLowerCase();
      return ch;
    })
    .replace(/([0-9])([a-z]+)([0-9])/ig, function(match, g1, g2, g3) {
      return g3 + g2 + g1;
    });
}