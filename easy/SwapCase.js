function SwapCase(str) {
  return str
    .replace(/[a-z]/gi, function(ch) {
      if (ch.match(/[a-z]/)) return ch.toUpperCase();
      if (ch.match(/[A-Z]/)) return ch.toLowerCase();
      return ch;
    });
}
