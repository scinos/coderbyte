function MeanMode(arr) {

  function getMode(arr) {
    var mode = 0;
    var count = {};

    for (var i = 0; i<arr.length; i++) {
      if (!count[arr[i]]) count[arr[i]]=1;
      else count[arr[i]]++;
    }

    var c = 0;
    for (var i in count) {
      if (count[i]>c && count[i]>1) {
        mode = Number(i);
        c = count[i]
      }
    }

    return mode;
  }

  function getMean(arr) {
    var total = 0;
    for (var i = 0; i<arr.length; i++) {
      total += arr[i];
    }

    return total / arr.length;
  }

  if (getMean(arr) == getMode(arr)) return 1;
  return 0;
}