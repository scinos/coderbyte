function FirstFactorial(num) {

  function fac(num) {
    if (!num) return 1
    else return num * fac(num-1)
  }

  return fac(num)
}