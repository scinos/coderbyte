function LetterChanges(str) {
  return str.replace(/[a-z]/g, function(l) {
    if (l == "z") return "a"
    return String.fromCharCode(l.charCodeAt(0) + 1)
  }).replace(/[aeiou]/g, function(l) {
    return l.toUpperCase()
  })
}