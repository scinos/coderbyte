function SimpleSymbols(str) {
  var len = str.length

  for (var i = 0; i<len; i++) {
    var l = str[i]
    var p = str[i-1]
    var n = str[i+1]

    if (l.match(/[a-z]/i) && (p !== "+" || n !== "+")) {
      return false
    }
  }

  return true
}