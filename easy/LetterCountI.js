function LetterCountI(str) {

  function maxLetter(word) {
    var letters = {}
    for (var i=0; i<word.length; i++) {
      var chr = word[i]
      if (chr in letters) letters[chr]++
      else letters[chr]=1
    }

    var max = 0
    for (var ch in letters) {
      max = Math.max(max, letters[ch])
    }
    return max
  }

  var biggestWord
  var biggestRepetition = 1
  var words = str.split(" ")
  for (var i=0; i<words.length; i++) {
    var word = words[i]
    var max = maxLetter(word)
    if (max>biggestRepetition) {
      biggestWord = word
      biggestRepetition = max
    }
  }

  if (biggestRepetition>1) return biggestWord
  return -1
}