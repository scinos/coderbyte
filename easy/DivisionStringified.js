function DivisionStringified(num1,num2) {

  var result = "" + Math.round(num1/num2)
  var commas = ""
  var temp = ""
  var reverse = result.split("").reverse().join("")

  for (var i = 0; i<reverse.length; i++) {
    commas = commas + reverse[i]
    temp = temp + reverse[i]
    if (temp.length % 4 == 0 && i<reverse.length-1) {
        commas = commas + ","
    }
  }

  return commas.split("").reverse().join("")
}