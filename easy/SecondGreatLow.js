function SecondGreatLow(arr) {
  arr.sort(function(a,b){return a-b})

  function second(arr) {
    var lowest = arr[0]
    for (var i=1;i<arr.length; i++) {
      if (arr[i]!=lowest) {
        lowest = arr[i]
        break
      }
    }
    return lowest
  }

  return second(arr) + " " + second(arr.reverse())
}