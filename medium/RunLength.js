function RunLength(str) {
  var result = ""
  var count = 0
  var lastChar = ""

  for (var i = 0; i<str.length; i++) {
    var chr = str[i]
    if (lastChar == "" || chr == lastChar) {
      count++
    }else{
      result += count + lastChar
      count = 1
    }
    lastChar = chr
  }
  result += count + lastChar

  return result
}