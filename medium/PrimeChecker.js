function PrimeChecker(num) {

  function permutations(str) {
    if (str.length == 1) {
      return [str];
    }else{
      var perms = [];
      var a = str.split("");
      for (var i = 0; i<a.length; i++) {
        var item = a[i];
        var newA = a.slice(0, i).concat(a.slice(i+1));
        var perA = permutations(newA.join(""));
        for (var h = 0; h<perA.length; h++) {
          perms.push(item + perA[h]);
        }
      }
      return perms;
    }
  }

  function isPrime(num) {
    if (num == 1) return false;

    for (var i=2; i<=num/2; i++) {
      if (num%i==0) return false
    }
    return true
  }

  var perms = permutations(String(num));

  for (var i = 0; i<perms.length; i++) {
    console.log(perms[i]);
    var num = Number(perms[i]);
    if (isPrime(num)) return 1
  }

  return 0;
}